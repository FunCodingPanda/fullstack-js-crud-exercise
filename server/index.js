const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const app = express()
const helpers = require('./helpers');
const knex = require('./db')

app.use(bodyParser.json());

var corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
};

app.get('/api/employees', cors(corsOptions), (req, res, next) => {
  console.log('GET /api/employees');
  res.setHeader('Content-Type', 'application/json');
  knex('employees').select().then((employees, error) => {
    if (error) {
      res.status(500);
      res.send(JSON.stringify({ error: 'Failed to get employees' }, null, 2));
    } else {
      res.status(200);
      res.send(JSON.stringify(employees, null, 2));
    }
  });
});

app.get('/api/employees/:id', cors(corsOptions), (req, res, next) => {
  console.log(`GET /api/employees/${req.params.id}`);
  res.setHeader('Content-Type', 'application/json');
  knex('employees').where('id', req.params.id).then((employee, error) => { 
    if (error) {
      res.status(500);
      res.send(JSON.stringify({ error: 'Failed to get employee' }, null, 2));
    } else {
      res.status(200);
      res.send(JSON.stringify(employee, null, 2));
    }
  });
});

app.post('/api/employees', cors(corsOptions), (req, res, next) => {
  console.log('POST /api/employees');
  console.log(req.body);
  res.setHeader('Content-Type', 'application/json');
  const newEmployee = {
    name: req.body.name,
    code: req.body.code,
    profession: req.body.profession,
    color: req.body.color,
    city: req.body.city,
    branch: req.body.branch,
    assigned: req.body.assigned,
  };

  const validationError = helpers.validateNewEmployee(newEmployee);
  if (validationError) {
    res.status(400);
    res.send(JSON.stringify({ error: validationError }, null, 2));
  } else {
    knex('employees')
      .returning('*')
      .insert(newEmployee).then((employees, error) => {
        if (error) {
          res.status(500);
          res.send(JSON.stringify({ error }, null, 2));
        } else {
          res.status(200);
          res.send(JSON.stringify(employees[0], null, 2));
        }
      });
    }
});

app.delete('/api/employees/:id', cors(corsOptions), (req, res, next) => {
  console.log(`DELETE /api/employees/${req.params.id}`);
  res.setHeader('Content-Type', 'application/json');

  knex('employees').where('id', req.params.id).delete().then((numDeleted) => { 
    if (numDeleted === 0) {
      res.status(404);
      res.send(JSON.stringify({ error: 'Failed to delete employee' }, null, 2));
    } else {
      res.status(204).end();
    }
  });
});

// Bonus
app.patch('/api/employees/:id', cors(corsOptions), (req, res, next) => {
  console.log(`PATCH /api/employees/${req.params.id}`);
  res.setHeader('Content-Type', 'application/json');

  const updatedError = helpers.validateEmployeeUpdate(req.body);
  if (updatedError) {
    res.status(400);
    res.send(JSON.stringify({ error: updatedError }, null, 2));
  } else {
    knex('employees')
      .returning('*')
      .where('id', req.params.id)
      .update(req.body).then(employees => {
        if (!employees[0]) {
          res.status(404);
          res.send(JSON.stringify({ error: 'Could not find employee' }, null, 2));
        } else {
          const updatedEmployee = {
            ...employees[0],
            ...req.body
          };
          res.status(200);
          res.send(JSON.stringify(updatedEmployee, null, 2));
        }
      });
  }
});

app.listen(8080, () => console.log('Job Dispatch API running on port 8080!'));