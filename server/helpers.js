const validateNewEmployee = (employee) => {
  if (!employee.name || typeof employee.name !== 'string') {
    return "Employee name must be a string";
  } else if (!employee.code || typeof employee.code !== 'string') {
    return "Employee code must be a string";
  } else if (!employee.profession || typeof employee.profession !== 'string') {
    return "Employee profession must be a string";
  } else if (!employee.color || typeof employee.color !== 'string') {
    return "Employee color must be a string";
  } else if (!employee.city || typeof employee.city !== 'string') {
    return "Employee city must be a string";
  } else if (!employee.branch || typeof employee.branch !== 'string') {
    return "Employee branch must be a string";
  } else if (typeof employee.assigned !== 'boolean') {
    return "Employee assignment must be a boolean";
  } else {
    return null;
  }
};

const validateEmployeeUpdate = (fields) => {
  const validFields = ['name', 'code', 'profession', 'color', 'city', 'branch', 'assigned'];
  for (field in fields) {
    if (!validFields.includes(field)) {
      return `Invalid field ${field}`;
    }
  }

  if (!fields.name && !fields.code && !fields.profession && !fields.color && !fields.city && !fields.branch && !fields.assigned) {
    return "Please insert one of: name, code, profession, color, city, branch, or assigned"
  }

  if (fields.name && typeof fields.name !== 'string') {
    return "Employee name must be a string";
  } else if (fields.code && typeof fields.code !== 'string') {
    return "Employee code must be a string";
  } else if (fields.profession && typeof fields.profession !== 'string') {
    return "Employee profession must be a string";
  } else if (fields.color && typeof fields.color !== 'string') {
    return "Employee color must be a string";
  } else if (fields.city && typeof fields.city !== 'string') {
    return "Employee city must be a string";
  } else if (fields.branch && typeof fields.branch !== 'string') {
    return "Employee branch must be a string";
  } else if (fields.assigned !== undefined && typeof fields.assigned !== 'boolean') {
    return "Employee assignment must be a boolean";
  } else {
    return null;
  }
};

module.exports = { validateNewEmployee, validateEmployeeUpdate };
