# Plexxis Interview Exercise
## Requirements
Create a simple but __impressive__ (looks good, works well, has intuitive design, etc.) CRUD application that can do the following:

1) Retrieve employees from a REST API  
2) Display the employees in a React application  
3) Has UI mechanisms for creating and deleting employees  
4) Has API endpoints for creating and deleting employees  
5) Edit your version of the `README.md` file to explain to us what things you did, where you focussed your effort, etc.

*Read over the `Bonus` objectives and consider tackling those items as well*

## Bonus (Highly Encouraged)

1) Use a relational database to store the data (SQLite, MariaDB, Postgres)  
2) UI mechanisms to edit/update employee data  
3) Add API endpoint to update employee data  
4) Use [React Table](https://react-table.js.org)  

## Getting Started
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). The front-end app runs off localhost:3000. The REST API is located in the /server folder and runs off localhost:8080. The data is being served from a JSON file located in the /server/data folder. Run `npm start` to start both servers.

## Getting it Done
* You are free to use whatever libraries that you want. Be prepared to defend your decisions.
* There is no time limit. Use as little or as much time as is necessary to showcase your abilities.
* You should fork or clone our repository into your own repository.
  * Send us the link when you are done the exercise (pglinker at plexxis dot com).

If you do well on the test, we will bring you in for an interview. Your test results will be used as talking points.  

 __This is your chance to amaze us with your talent!__

# Instructions

Do the usual `yarn install` to install dependencies, and run `npm install -g knex` to install the knex database tool.

Then ensure you have postgres installed (usually `brew install postgresql`), and run `createdb employees`.

Next, run migrations and seeds with `knex migrate:latest` and `knex seed:run` respectively.

Then, `yarn start` will start the server for development.

# What I did

## Frontend
1. Incorporated React Table with the employees data.
2. Made the "New Employee" button & attached that to a modal. Modal immediately adds the new employee to the table after the completion of the form.
3. Made a "Delete" button for deleting employees.
4. Implement intuitive UI by having buttons change background or text color when hovering in addition to letting the user know if the a new employee is added, an employee is deleted or updated. Altered the typeface to Helvetica Neue since it's easier to read.
5. Created the "Edit" button for updating employee info.

## Backend
1. Focused on implementing CRUD starting with GET, GET BY ID, POST, DELETE & then the PATCH.
2. For the POST, I validated the field according to its data type. I separated the validation field into a different file called "helpers.js".
3. In the helpers file, I also included valid update fields to assure none of the fields are missing and all the pre-set fields are there.
4. I connected a Postgres database to the Express framework using Knex. Then I worked on migrations and seeds.
5. Added a proxy field to the configuration in the package.json file to allow CORS to function properly when the API and static assets are on different localhost ports.

