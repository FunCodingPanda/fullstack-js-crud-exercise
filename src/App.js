import React from 'react';
import ReactTable from "react-table";
import "./App.css";
import Modal from "./Modal.js"

const defaultEmployee = {
  name: "",
  code: "",
  profession: "",
  color: "",
  city: "",
  branch: "",
  assigned: true
};

class App extends React.Component {
  state = {
    employees: [],
    editing: defaultEmployee,
    alertMessage: ""
  }
  
  componentWillMount = () => {
    fetch('/api/employees')
      .then(response => response.json())
      .then(employees => this.setState({ employees }))
  }

  componentDidMount = () => {
    window.$('#alert').hide();
  }

  createEmployee = (employee) => {
    fetch('/api/employees', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(employee)
    }).then(response => response.json())
    .then(employee => {
      this.setState(state => ({
        ...state,
        employees: [...state.employees, employee]
      }));
      window.$('#createModal').modal('hide');
      this.alert("Employee was created.");
    });
  }

  deleteEmployee = (id) => {
    if (window.confirm('Are you sure you want to delete this employee?') === true) {
      fetch(`/api/employees/${id}`, {
        method: 'DELETE',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      }).then(() => {
        this.setState({ employees: this.state.employees.filter(e => e.id !== id) });
        this.alert("Employee was deleted.");
      });
    }
  }

  editEmployee = (employee) => {
    fetch(`/api/employees/${employee.id}`, {
      method: 'PATCH',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({...employee, id: undefined})
    }).then(response => response.json())
    .then(updatedEmployee => {
      this.setState(state => ({
        ...state,
        employees: state.employees.map(e => {
          if (e.id === employee.id) {
            return updatedEmployee;
          } else {
            return e;
          }
        })
      }));
      window.$('#editModal').modal('hide');
      this.alert("Employee was updated.");
    });
  }

  alert = (alertMessage) => {
    this.setState({ alertMessage });
    setTimeout(() => window.$('#alert').show(), 10);
    setTimeout(() => {
      this.setState({ alertMessage: "" });
      window.$('#alert').hide();
    }, 4000);
  }

  render() {
    const {
      employees
    } = this.state;

    console.log(this.state);

    const columns = [
      {
        Header: "Name",
        accessor: "name"
      },
      {
        Header: "Code",
        accessor: "code"
      },
      {
        Header: "Profession",
        accessor: "profession"
      },
      {
        Header: "Color",
        accessor: "color"
      },
      {
        Header: "City",
        accessor: "city"
      },
      {
        Header: "Branch",
        accessor: "branch"
      },
      {
        Header: "Assigned",
        id: "assigned",
        accessor: d => d.assigned.toString()
      },
      {
        Header: "",
        id: "actions",
        maxWidth: 115,
        accessor: d => (
          <div>
            <button
              className="editButton"
              data-toggle="modal"
              data-target="#editModal"
              onClick={e => this.setState({ editing: d })}>
              Edit
            </button>
            <button
              className="deleteButton"
              onClick={e => this.deleteEmployee(d.id)}>
              Delete
            </button>
          </div>
        )
      }
    ];

    return (
      <div className="App">
        <div id="alert" class="alert alert-success alert-dismissible fade show" role="alert">
          {this.state.alertMessage}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        
        <div className="header">
          <h1>Plexxis Employees</h1>
          <button type="button" className='newEmployee' data-toggle="modal" data-target="#createModal"> New Employee </button>
        </div>
        <Modal
          title="New Plexxis Employee"
          name="createModal"
          onSave={this.createEmployee}
          onClose={() => this.setState({ editing: defaultEmployee })}
          onEdit={props => this.setState(state => ({...state, editing: {...state.editing, ...props} }))}
          employee={this.state.editing}
        />
        <Modal
          title="Edit Plexxis Employee"
          name="editModal"
          onSave={this.editEmployee}
          onClose={() => this.setState({ editing: defaultEmployee })}
          onEdit={props => this.setState(state => ({...state, editing: {...state.editing, ...props} }))}
          employee={this.state.editing}
        />
        <ReactTable
          data={employees}
          columns={columns}    
          defaultPageSize={10}
        />
      </div>
    );
  }
}

export default App;
