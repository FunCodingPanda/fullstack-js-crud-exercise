import React, { Component } from 'react';

class Modal extends Component {
  constructor(props) {
    super(props);
  }

  save = () => {
    this.props.onSave(this.props.employee);
  }

  render () {
    const {
      employee,
      onClose = () => {}
    } = this.props;

    return (
      <div className="modal fade" id={this.props.name} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h3 className="modal-title" id="exampleModalLabel">{this.props.title}</h3>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={e => this.props.onClose()}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

              <div className="modal-body">
                <form>
                  <div className="form-group">
                    <label>Full Name</label>
                    <input type="text"
                      className="form-control"
                      placeholder="Amy Riemer"
                      value={employee.name}
                      onChange={e => this.props.onEdit({ name: e.target.value })} />
                    </div>
                    <div className="form-group">
                    <label>Code</label>
                    <input type="text" 
                           className="form-control" 
                           placeholder="F101"
                           value={employee.code}
                           onChange={e => this.props.onEdit({ code: e.target.value })} />        
                    </div>
                    <div className="form-group">
                    <label>Profession</label>
                    <input type="text" 
                           className="form-control" 
                           placeholder="Drywall Installer"
                           value={employee.profession}
                           onChange={e => this.props.onEdit({ profession: e.target.value })} />
                    </div>
                    <div className="form-group">
                    <label>Color</label>
                    <input type="text" 
                           className="form-control" 
                           placeholder="Green"
                           value={employee.color}
                           onChange={e => this.props.onEdit({ color: e.target.value })} />             
                    </div>
                    <div className="form-group">
                    <label>City</label>
                    <input type="text" 
                           className="form-control" 
                           placeholder="Toronto"
                           value={employee.city}
                           onChange={e => this.props.onEdit({ city: e.target.value })} />
                    </div>
                    <div  className="form-group">
                    <label>Branch</label>
                    <input type="text" 
                           className="form-control"
                           placeholder="Security"
                           value={employee.branch}
                           onChange={e => this.props.onEdit({ branch: e.target.value })} />              
                           </div>

                  <div onChange={e => this.props.onEdit({ assigned: (e.target.value == 'true') })}>
                    <div className="form-check form-check-inline">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="inlineRadioOptions"
                        value="true"
                        defaultChecked={employee.assigned}
                      />
                      <label className="form-check-label">True</label>
                    </div>
                    <div className="form-check form-check-inline">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="inlineRadioOptions"
                        value="false"
                        defaultChecked={!employee.assigned}
                      />
                      <label className="form-check-label">False</label>
                    </div>
                  </div>

                </form>
              </div>

            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={e => this.props.onClose()}>Close</button>
              <button type="button" className="btn btn-light" onClick={ e => this.save() }>Save changes</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Modal;

