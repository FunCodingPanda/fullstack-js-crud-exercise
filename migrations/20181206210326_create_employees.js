exports.up = (knex) => {
  return knex.schema.createTable('employees', table => {
    table.increments()
    table.string('name')
    table.string('code')
    table.string('profession')
    table.string('color')
    table.string('city')
    table.string('branch')
    table.boolean('assigned')
  })
}

exports.down = (knex) => {
  return knex.schema.dropTableIfExists('employees')
}
