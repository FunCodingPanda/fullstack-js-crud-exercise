module.exports = {
  development: { client: 'pg', connection: { database: 'employees' } },
  test: { client: 'pg', connection: { database: 'employees' } },
  production: {
    client: 'postgresql',
    connection: process.env.DATABASE_URL,
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }
};